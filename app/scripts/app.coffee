  pluginName = "selectbox"
  defaults =
    placeholder: "Select items"
    inline: false
    multiple: false
    disabled: false
    groups: false
    width: null
    height: null
    tabindex: 0
    accesskey: "w"
    class:
      wrapper: pluginName
      placeholder: "psPlaceholder"
      element: "psElement"
      head: "psHead"
      filter: "psFilter"
      result: "psResult"
      inner: "psInner"
      group: "psGroup"
      option: "psOption"
      filtered: "psFiltered"
    filter:
      enable: false
      in: "options"
      placeholder: "Type to filter"
      focus: false
      result: true
      template: "Showed {{filtered}} of {{count}}"
    head:
      template: "{{options}}"
      separator: ", "
      overflow: 3
      overflowTemplate: "Selected {{selected}} of {{count}}"
      allTemplate: "All selected ({{count}})"
    onOpen: $.noop
    onClose: $.noop
    onSelect: $.noop
    onUnselect: $.noop
    onSelectGroup: $.noop
    onUnselectGroup: $.noop
    onFilterChange: $.noop
    onFilterResult: $.noop

  class Plugin
    constructor: (@element, options) ->
      if options && options.filter is yes
        options.filter = {}
        options.filter.enable = true
      @settings = $.extend true, {}, defaults, options
      @_defaults = defaults
      @_name = pluginName
      @_init()

    _init: ->
      @el = $ @element
      @settings.multiple = @el.prop("multiple") || @settings.multiple unless @settings.inline
      @settings.disabled = @el.is(":disabled") || @settings.disabled 
      @settings.tabindex = @el.prop("tabindex") || @settings.tabindex 
      @_build()

    _build: ->
      @cover = $()
        .add(@_filter())
        .add(@_inner())
        .add(@_result())
        .wrapAll("<div class='psCover'/>")
        .parent()

      @el
        .addClass @settings.class.element
        .attr "tabIndex", -1
        .wrap @_wrapper()
        .after @_head(), @cover

      return if @settings.disabled

      @_initItems()
      @_delegateEvents()
      @inner.scrollbar({autoUpdate: true, disableBodyScroll: true}) if $.fn.scrollbar

      @wrapper.css("maxWidth", @settings.width) if @settings.width
      $("." + @settings.class.inner).css("maxHeight", @settings.height) if @settings.height
      @wrapper.addClass "inline" if @settings.inline
      

    _wrapper: ->
      cls = [@settings.class.wrapper]
      cls.push "disabled" if @settings.disabled
      @wrapper = $("<div/>").addClass cls.join(" ")

    _head: ->
      cls = [
        @settings.class.head,
        @settings.class.placeholder
      ]
      @head = $ "<span/>"
        .addClass cls.join(" ")
        .text @settings.placeholder
        .attr "tabindex", @settings.tabindex
        .attr "accesskey", @settings.accesskey

    _filter: ->
      if @settings.filter.enable
        @filterInput = $ "<input/>"
          .attr "type", "search"
          .attr "placeholder", @settings.filter.placeholder

        @filter = $ "<span/>"
          .addClass @settings.class.filter
          .wrapInner @filterInput

    _result: ->
      if @settings.filter.enable && @settings.filter.result
        @result = $ "<span/>"
          .addClass @settings.class.result

    _createItem: (group, value, cls, label) =>
      $ "<span/>"
        .addClass(cls.join(" "))
        .attr "data-group", group
        .attr "data-value", value
        .html label

    _inner: ->
      items = $()
      group = 0
      self = this
      @el.find("option, optgroup").each ->
        cls = []
        cls.push if @selected then "selected" else null
        cls.push if @disabled then "disabled" else null
        label = @label || @text
        item = if @tagName.toLowerCase() == "optgroup"
          cls.push self.settings.class.group
          value = (@id || label).toLowerCase()
          self._createItem(++group, value, cls, label)
        else
          cls.push self.settings.class.option
          self._createItem(group, @value, cls, label)
        items = items.add item

      @inner = $ "<div/>"
        .addClass @settings.class.inner
        .attr "tabIndex", 2
        .append items
      
    _initItems: =>
      @wrapper = @el.parent()
      @items = @inner.children()
      @options = @items.filter "." + @settings.class.option
      @groups = @items.filter "." + @settings.class.group
      @groups.addClass "clickable" if @settings.groups and @groups
      @selected = @options.filter ".selected"
      if @selected.length > 0
        @selected.each (index, item) =>
          @_set_selected_option($ item)
        @_setHead()
      @_initFilter()

    _initFilter: =>
      if @settings.filter.enable
        @search = switch @settings.filter.in
          when "options" then @options
          when "groups" then @groups
          when "all" then @items
        if @settings.filter.result
          @result.text @_parseTemplate(@settings.filter.template)

    _delegateEvents: (items) =>
      $("." + @settings.class.wrapper)
        # Item events
        .on "click.ps", "." + @settings.class.option, @onClickItem
        .on "click.ps", "." + @settings.class.group,  @onClickGroup
        # Head events
        .on "open.ps",  "." + @settings.class.head,   @open
        .on "close.ps", "." + @settings.class.head,   @close
        .on "touchstart.ps click.ps", "." + @settings.class.head, @onClick
        # Filter input events
        .on "change.ps", "." + @settings.class.filter + " input", @onFilterChange
        .on "keyup.ps",  "." + @settings.class.filter + " input", @onFilterKeyup
        .on "click.ps",  "." + @settings.class.filter + " input", @onFilterClick

    onClick: =>
      if @wrapper.hasClass "open"
        @head.trigger "close.ps"
      else
        @head.trigger "open.ps"

    open: =>
      @wrapper.addClass "open"

      if @filter
        @filter.css "display", "block"
        @filterInput.focus() if @settings.filter.focus

      @result.css "display", "block" if @result
      @settings.onOpen.call(@wrapper)

    close: =>
      @wrapper.removeClass "open"
      @filter.hide() if @filter
      @result.hide() if @result
      @settings.onClose.call(@wrapper)

    destroy: ->
      select = @el
        .removeClass @settings.class.element
      @wrapper
        .after select
        .off()
        .remove()
        .removeData()

    # to array of case insensitive string indexes
    _parseIndex: (index) =>
      separator = "^"
      index = switch typeof index
        when "object", "array"
          index.join(separator)
        else
          index.toString()
      index.toLowerCase().split(separator)

    _getElements: (type, index) =>
      switch type
        when "option", "options"
          @type = "option"
          elements = @options
        when "group", "groups"
          @type = "group"
          elements = @groups
      if index
        index = @_parseIndex(index) 
        elements.filter ->
          index.indexOf(this.getAttribute("data-value")) > -1
      else
        return elements

    set: (type, index, value) =>
      self = this
      index = [value, value = index][0] if typeof value == "undefined"
      elements = @_getElements(type, index)
      method = "_set_" + value.toLowerCase() + "_" + @type 
      return unless Plugin.prototype.hasOwnProperty(method)
      elements.map ->
        eval("self." + method + "($(this))")
      @_setHead()
      elements

    get: (type, index) =>
      elements = @_getElements(type, index)
      if @type == "group"
        options = $()
        elements.each (i, item) =>
          el = $ item
          options = options.add el
          options = options.add @_getGroupElements(el)
        options
      else
        elements

    _objectToItems: (group, items) =>
      structure = ["name", "value", "selected", "disabled", "options"]
      result = $()

      for key, option of items
        cls = []
        cls.push "selected" if option.selected
        cls.push "disabled" if option.disabled
        if option.options
          cls.push @settings.class.group
          ++group
        else
          cls.push @settings.class.option
        result = result.add @_createItem(group, option.value, cls, option.name)
        result = result.add @_objectToItems(group, option.options) if option.options
      result

    _getMaxIndex: =>
      arr = []
      @items.map ->
        arr.push this.getAttribute("data-group")
      Math.max.apply(Math, arr)

    _createNativeOption: (item) =>
      option = $ "<option/>"
        .attr "value", item.value
        .text item.name
      option.prop("selected", true) if item.selected
      option.prop("disabled", true) if item.disabled
      option

    _addNativeOptions: (items) ->
      html = $()
      for key, item of items
        html = html.add if item.options
          options = $()
          for k, child of item.options
            options = options.add @_createNativeOption(child)
          $("<optgroup/>")
            .attr "id", item.value
            .attr "label", item.name
            .html options
        else
          @_createNativeOption(item)
      @el.append html


    add: (items, dest, type, index) =>
      return unless typeof items is "object"
      group = @_getMaxIndex() + 1
      @_addNativeOptions(items)
      items = @_objectToItems(group, items)
      if index
        el = @_getElements(type, index)
        el = el.last() if el.length > 1
        if dest is "before"
          el.before items
        else
          if type is "group"
            options = @_getGroupElements(el)
            el = options.last() if options
          el.after items
      else
        if dest is "prepend"
          @inner.prepend(items)
        else
          @inner.append(items)

      @_initItems()
      items

    remove: (type, index) =>
      elements = @get(type, index)
      @items.filter(elements).remove()
      @items = @items.not(elements)
      @_initItems()

    onFilterChange: =>
      val = @filterInput.val()
      filter = val.toLowerCase()
      results = []
      values = 0
      @settings.onFilterChange.call(@filterInput, val)
      return if @lastFilter == filter
      @search.each (key, item) =>
        $el = $(item)
        if $el.text().toLowerCase().indexOf(filter) >= 0
          values += parseInt($el.attr "data-value") if $el.attr "data-value"
          results.push item

      if values != @lastValues && results.length > 0
        @filtered = $ results
        @settings.onFilterResult.call(@filterInput, @filtered)
        @items
          .removeClass @settings.class.filtered
          .hide()
        @filtered.addClass @settings.class.filtered
        @lastValues = values
        if @settings.filter.result
          @result.text @_parseTemplate(@settings.filter.template)
      @lastFilter = filter

    onFilterKeyup: (e) =>
      switch e.which
        when 27 then @filterInput.trigger "blur"
        else @filterInput.trigger "change.ps"

      @items.show() unless @filterInput.val()

    # trigger keyup if input[search] clear button clicked
    onFilterClick: =>
      setTimeout (=>
        if !@filterInput.val() && @lastFilter
          @filterInput.trigger "keyup.ps"
      ), 10

    onClickItem: (e) =>
      el = $(e.target)
      index = $(@options).index(el)
      return if el.hasClass "disabled"
      if @settings.multiple
        group = el.data("group")
        if el.hasClass "selected"
          @_set_unselected_option(el)
          if group
            @groups
              .filter("[data-group=" + group + "]")
              .removeClass "selected" 
        else
          @_set_selected_option(el)
          if group
            items = @options.filter("[data-group=" + group + "]:not(.disabled)")
            if items.length == items.filter(".selected").length
              @groups
                .filter("[data-group=" + group + "]")
                .addClass "selected"
      else
        @head.trigger "close.ps"
        if !@last
          @_set_selected_option(el)
        else if el.get(0) != @last.get(0)
          @last.removeClass "selected"
          @_set_unselected_option(@last)
          @_set_selected_option(el)
        else
          return
      @_setHead()

    onClickGroup: (e) =>
      el = $(e.target)
      return if el.hasClass("disabled") || !el.hasClass("clickable")
      if el.hasClass "selected"
        @_set_unselected_group el
      else
        @_set_selected_group el
      @_setHead()

    _getGroupElements: (el) =>
      index = el.data("group")
      @options.filter("[data-group=" + index + "]")

    _set_selected_group: (el) =>
      options = @_getGroupElements(el).map (i, item) =>
        @_set_selected_option($ item)
      el.addClass "selected"
      @settings.onSelectGroup.call(el, options)

    _set_unselected_group: (el) =>
      options = @_getGroupElements(el).map (i, item) =>
        @_set_unselected_option($ item)
      el.removeClass "selected"
      @settings.onUnselectGroup.call(el, options)

    _set_selected_option: (el) ->
      option = el.text()
      value = el.data("value")
      unless el.hasClass "disabled"
        el.addClass "selected"
        @el
          .find("[value=" + value + "]")
          .attr("selected", true)
        unless @selected.is(el)
          @selected = @selected.add(el)
          @settings.onSelect.call(el, option, value)
        @last = el

    _set_unselected_option: (el) ->
      option = el.text()
      value = el.data("value")
      el.removeClass "selected"
      @el
        .find("[value=" + value + "]")
        .attr("selected", false)
      if @selected.is(el)
        @selected = @selected.not(el)
        @settings.onUnselect.call(el, option, value)

    _set_enabled_group: (el) ->
      @_getGroupElements(el).removeClass "disabled"
      el.removeClass "disabled"

    _set_disabled_group: (el) ->
      @_getGroupElements(el).addClass "disabled"
      el.addClass "disabled"

    _set_enabled_option: (el) ->
      el.removeClass "disabled"

    _set_disabled_option: (el) ->
      el.addClass "disabled"

    _setHead: ->
      opts = @settings.head
      if @selected.length == @options.length
        tpl = opts.allTemplate
      else if opts.overflow > 0 && @selected.length >= opts.overflow
        tpl = opts.overflowTemplate
      else
        tpl = opts.template
      if head = @_parseTemplate(tpl, opts.separator)
        @head.removeClass "psPlaceholder"
      else
        head = @settings.placeholder
        @head.addClass "psPlaceholder"
      @head.text(head)

    _parseTemplate: (tpl, separator) ->
      head = @selected.map ->
        $(this).text()

      vars =
        count: @options.length 
        options: head.toArray().join(separator)
        selected: @selected.length

      vars.filtered = if @filtered then @filtered.length else vars.count
      tpl.replace /\{\{(.*?)\}\}/g, (i, match) ->
        return vars[match]

  $.fn[pluginName] = (options) ->
    args = arguments
    if options == undefined or typeof options == "object"
      return @each ->
        unless $.data(this, "plugin_" + pluginName)
          $.data this, "plugin_" + pluginName, new Plugin(this, options)
    else if typeof options == "string" and options[0] != "_"
      returns = undefined
      @each ->
        instance = $.data(this, "plugin_" + pluginName)
        if instance instanceof Plugin and typeof instance[options] == "function"
          returns = instance[options].apply(instance, Array::slice.call(args, 1))
        if options == "destroy"
          $.data this, "plugin_" + pluginName, null
        return
      return if returns != undefined then returns else this