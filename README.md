jQuery Selectbox Plugin
==================================
### Install npm dependencies

```
npm install
```

This runs through all dependencies listed in `package.json` and downloads them
to a `node_modules` folder in your project directory.

### Install bower dependencies

```
bower install
```

### Run gulp

```
gulp
gulp build # production
```