gulp         = require "gulp"
params       = require "./config/parameters.coffee"
coffee       = require "gulp-coffee"
concat       = require "gulp-concat"
gutil        = require "gulp-util"
# path         = require "path"
rename       = require "gulp-rename"
# less         = require "gulp-less"
uglify       = require "gulp-uglify"
cssmin       = require "gulp-cssmin"
autoprefixer = require "gulp-autoprefixer"
amdOptimize  = require "amd-optimize"
watch        = require "gulp-watch"
plumber      = require "gulp-plumber"
wiredep      = require("wiredep").stream
browserSync  = require "browser-sync"
notify       = require "gulp-notify"
reload       = browserSync.reload

gulp

  .task "scripts", ->
    gulp.src params.scripts + "/*.coffee"
    .pipe plumber()
    .pipe coffee bare: true
    .pipe amdOptimize "app", configFile: gulp.src(params.scripts + "config.coffee").pipe(coffee())
    .pipe concat "scripts.js"
    .pipe gulp.dest params.dist
    .on "error", gutil.log

  .task "scripts/vendor", ->
    gulp.src params.scripts + "vendor/**/*.js"
      .pipe gulp.dest  params.dist
      .on "error", gutil.log

  .task "styles", ->
    gulp.src params.styles + "/*.css" # .less
    # .pipe less paths: [ path.join(__dirname) ]
    .pipe autoprefixer browsers: "last 2 versions", cascade: false
    .pipe rename "styles.css"
    .pipe gulp.dest params.dist
    .on "error", gutil.log

  .task "styles/vendor", ->
    gulp.src params.styles + "vendor/**/*.css"
      .pipe gulp.dest params.dist + "vendor/"
      .on "error", gutil.log

  .task "minify", ["scripts", "scripts/vendor"], ->
    gulp.src params.dist + "**/*.js"
      .pipe concat "scripts.js"
      .pipe uglify()
      .pipe gulp.dest params.build

  .task "cssmin", ["styles", "styles/vendor"], ->
    gulp.src params.dist + "**/*.css"
      .pipe concat "styles.css"
      .pipe cssmin()
      .pipe gulp.dest params.build

  .task "markup", ->
    gulp.src params.app + "index.html"
      .pipe wiredep directory: "./bower_components/"
      .pipe gulp.dest params.dist

  .task "dist", ["scripts", "scripts/vendor", "styles", "styles/vendor", "markup"]
    
  .task "build", ["minify", "cssmin", "markup"], ->
    gulp.src params.dist + "index.html"
      .pipe gulp.dest params.build

  .task "serve", ["dist"], ->
    browserSync server:
      baseDir: params.dist
      routes: "/bower_components": "bower_components"

    gulp.watch params.app + "*.html", ["markup"]
      .on "change", reload
    gulp.watch params.scripts + "**/*.coffee", ["scripts"]
      .on "change", reload
    gulp.watch params.styles + "**/*.css", ["styles"]
      .on "change", reload

  .task "default", ["serve"]