app = "./app/"
cfg = 
  app: app
  build: "build/"
  dist: "dist/"
  tests: "tests/"
  scripts: app + "scripts/"
  styles: app + "styles/"

module.exports = cfg